execute at @e[scores={PlayerNear=0},tag=path] run fill ~1 ~1 ~1 ~-1 ~-1 ~-1 minecraft:air
execute at @e[scores={PlayerNear=0}] if entity @a[distance=8..] run kill @e[type=minecraft:boat,distance=..2]
execute as @e[tag=path] at @s store success score @s PlayerNear run execute if entity @a[distance=..8]

execute at @e[scores={PlayerNear=1},tag=even] run fill ~ ~ ~1 ~ ~ ~-1 minecraft:quartz_slab
execute at @e[scores={PlayerNear=1},tag=even] run fill ~-1 ~ ~1 ~-1 ~ ~-1 minecraft:quartz_slab[type=top]
execute at @e[scores={PlayerNear=1},tag=even] run fill ~1 ~-1 ~1 ~1 ~-1 ~-1 minecraft:quartz_slab[type=top]

execute at @e[scores={PlayerNear=1},tag=odd] run fill ~ ~ ~1 ~ ~ ~-1 minecraft:quartz_slab[type=top]
execute at @e[scores={PlayerNear=1},tag=odd] run fill ~-1 ~1 ~1 ~-1 ~1 ~-1 minecraft:quartz_slab
execute at @e[scores={PlayerNear=1},tag=odd] run fill ~1 ~ ~1 ~1 ~ ~-1 minecraft:quartz_slab

execute at @e[scores={PlayerNear=1},tag=ice] run fill ~1 ~ ~1 ~-1 ~ ~-1 minecraft:blue_ice
execute at @e[scores={PlayerNear=1},tag=ice,tag=!transition] run fill ~1 ~1 ~1 ~1 ~1 ~-1 minecraft:spruce_trapdoor[facing=west,half=bottom,open=true]
execute at @e[scores={PlayerNear=1},tag=ice] run fill ~-1 ~1 ~1 ~-1 ~1 ~-1 minecraft:spruce_trapdoor[facing=east,half=bottom,open=true]
execute at @e[scores={PlayerNear=1},tag=ice,tag=transition] run setblock ~-1 ~1 ~-1 minecraft:spruce_stairs[facing=north,shape=inner_left]
execute at @e[scores={PlayerNear=1},tag=ice,tag=transition] run fill ~ ~1 ~-1 ~1 ~1 ~-1 minecraft:spruce_trapdoor[facing=south,half=bottom,open=true]
execute at @e[scores={PlayerNear=1},tag=ice,tag=transition] unless entity @e[type=minecraft:boat,distance=..6] run summon minecraft:boat ~ ~1 ~ {Type:"spruce"}
