execute at @e[tag=pathmaker] run summon minecraft:armor_stand ~ ~ ~ {NoGravity:1,Marker:1,Invisible:1,Tags:["path","ice"]}
execute as @e[tag=pathmaker] at @s run tp ~ ~ ~3
scoreboard players remove @e[tag=pathmaker] IcePathLength 1
execute unless score @e[tag=pathmaker,limit=1] IcePathLength matches -1 run function pathmaker:loadicepath
execute if score @e[tag=pathmaker,limit=1] IcePathLength matches -1 run kill @e[tag=pathmaker]
